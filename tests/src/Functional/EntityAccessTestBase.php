<?php

namespace Drupal\Tests\entity_access_by_reference_field\Functional;

use Drupal\entity_access_by_reference_field\Helper\Constants;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Provides methods for access testing entity_access_by_reference_field.
 *
 * @group entity_access_by_reference_field
 */
abstract class EntityAccessTestBase extends BrowserTestBase {
  use EntityReferenceFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user with all permissions.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * An authenticated user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $authenticatedUser;

  /**
   * A user with the permission to skip field check.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $globalPermissionUser;

  /**
   * The field storage.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'test_page_test',
    'entity_access_by_reference_field',
    'field',
    'field_ui',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);
    $this->config('system.site')->set('page.front', '/test-page')->save();

    // Create the adminUser:
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();

    // Create the global permissionUser:
    $this->globalPermissionUser = $this->createUser(['bypass entity_access_by_reference_field permissions']);

    // Create the authenticatedUser:
    $this->authenticatedUser = $this->createUser([]);

    // Revoke for both anonymous and authenticated user the ability to view
    // media and node entities:
    $authenticatedRole = Role::load('authenticated');
    $anonymousRole = Role::load('anonymous');
    $authenticatedRole->revokePermission('access content')->revokePermission('view media')->save();
    $anonymousRole->revokePermission('access content')->revokePermission('view media')->save();
  }

  /**
   * Creates the permission matrix.
   */
  public function initiatePermissionMatrix() {
    return [
      'columns' => [
        'update' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
        ],
        'view' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
        ],
        'view_unpublished' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
        ],
        'delete' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
        ],
      ],
    ];
  }

  /**
   * Creates the permission matrix with additional "is_referenced_user" entry.
   */
  public function initiatePermissionMatrixWithUserValues() {
    return [
      'columns' => [
        'update' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
          'is_referenced_user' => FALSE,
        ],
        'view' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
          'is_referenced_user' => FALSE,
        ],
        'view_unpublished' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
          'is_referenced_user' => FALSE,
        ],
        'delete' => [
          'delete' => FALSE,
          'view' => FALSE,
          'view_unpublished' => FALSE,
          'update' => FALSE,
          'is_referenced_user' => FALSE,
        ],
      ],
    ];
  }

  /**
   * Setup field storage third party settings.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $fieldStorage
   *   The field storage to set the third party settings on.
   * @param string $multipleEntityBehavior
   *   The multiple entity behavior.
   * @param bool $enable
   *   Whether or not to enable this modules functionality.
   * @param array $permissionMatrixValues
   *   The permission matrix values to set.
   * @param string $accessFallback
   *   The access fallback.
   * @param string $accessFallbackEmpty
   *   The empty access fallback.
   */
  public function setupThirdPartySettings(FieldStorageConfig $fieldStorage, string $multipleEntityBehavior = 'or', bool $enable = TRUE, array $permissionMatrixValues = [], string $accessFallback = Constants::FALLBACK_NEUTRAL, string $accessFallbackEmpty = Constants::FALLBACK_NEUTRAL) {
    // Setup the modules third party settings.
    $fieldStorage->setThirdPartySetting('entity_access_by_reference_field', 'multiple_entities_behavior', $multipleEntityBehavior);
    $fieldStorage->setThirdPartySetting('entity_access_by_reference_field', 'enable_referenced_entity_permission', $enable);
    $fieldStorage->setThirdPartySetting('entity_access_by_reference_field', 'permission_matrix', $permissionMatrixValues);
    $fieldStorage->setThirdPartySetting('entity_access_by_reference_field', 'access_fallback', $accessFallback);
    $fieldStorage->setThirdPartySetting('entity_access_by_reference_field', 'access_fallback_empty', $accessFallbackEmpty);
    $fieldStorage->save();
  }

}
